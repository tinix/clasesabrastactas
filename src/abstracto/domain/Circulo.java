package abstracto.domain;

/**
 *
 * @author tinix
 */
public class Circulo extends FiguraGeometrica {
    
    public Circulo(String tipoFigura) {
        super(tipoFigura); // llamamos el contructor protected de la clase padre
    }
    
    public void dibujar(){
        System.out.println("Aqui deberia dibjar un : " + this.getClass().getSimpleName());
    }
    
}
