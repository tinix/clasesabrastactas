package abstracto.domain;

/**
 *
 * @author tinix
 */
public class Rectangulo extends FiguraGeometrica {
    
    public Rectangulo(String tipoFigura){
        super(tipoFigura);
    }
    
    public void dibujar(){
        System.out.println("Aque deberia dibujar un :" + this.getClass().getSimpleName());
    }
    
}
