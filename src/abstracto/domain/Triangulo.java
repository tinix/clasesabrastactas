package abstracto.domain;

/**
 *
 * @author tinix
 */
public class Triangulo extends FiguraGeometrica {
    
    public Triangulo(String tipoFigura){
        super(tipoFigura);
    }
    
    public void dibujar(){
        System.out.println("Aque deberia dibujar un :" + this.getClass().getSimpleName());
    }
    
}
