package ejempleclasesabstractas;

import abstracto.domain.Rectangulo;
import abstracto.domain.FiguraGeometrica;
import abstracto.domain.Circulo;
import abstracto.domain.Triangulo;

/**
 *
 * @author tinix
 */

public class EjempleClasesAbstractas {

    public static void main(String[] args) {
      FiguraGeometrica rectangulo = new Rectangulo("Rectangulo");
      FiguraGeometrica triangulo = new Triangulo("Triangulo");
      FiguraGeometrica circulo = new Circulo("Circulo");
      
        System.out.println("rectangulo");
        rectangulo.dibujar();
        
        System.out.println("");
        System.out.println("triangulo");
        triangulo.dibujar();
        
        System.out.println("");
        System.out.println("circulo");
        circulo.dibujar();
    }
}
